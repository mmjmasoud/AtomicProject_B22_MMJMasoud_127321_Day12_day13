


<!DOCTYPE html>
<html lang="en">
<head>
  <title>Book</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
</head>
<body>

<div class="container">
  <h2>Book form</h2>
  <form action="Store.php" method="post" role="form">
    <div class="form-group">
      <label class="control-label col-sm-2">Book Name:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" name="title" id="text" placeholder="Enter book name">
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default">Submit</button>
      </div>
    </div>
  </form>
</div>

</body>
</html>
