<?php

include_once '../../../vendor/autoload.php';
use \App\Bitm\Seip127321\Book\Book;

$book = new Book();
$book->prepare($_POST);
$book->store();
